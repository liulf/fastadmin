<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;
use app\common\model\Category;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
//        $category = Category::get(5);
//        $this->view->assign("name", $category->name);
        return $this->view->fetch();
    }

    public function news()
    {
        $newslist = [];
        return jsonp(['newslist' => $newslist, 'new' => count($newslist), 'url' => 'https://www.fastadmin.net?ref=news']);
    }

    // llf test
    public function test($a="s"){
        $vars = \think\Lang::load(dirname(__DIR__) . DS . 'lang/zh-cn.php');
        $replace = array_values($vars);
        foreach ($replace as &$v) {
            $v = "{:{$v}}";
        }

        $c = include dirname(__DIR__) . DS . "lang/zh-cn1.php";
        $e = ["c"=>1];
        $c = $c + $e;


        dump($c);

    }

}
